package router

import (
	"testing"
	"reflect"
)

func TestStripEndingSlash(t *testing.T) {
	var stripped string

	stripped = stripEndingSlash("/")
	if stripped != "/" {
		t.Errorf("The root path has been stripped")
	}

	stripped = stripEndingSlash("/test/")
	if stripped != "/test" {
		t.Errorf("/test/ should be stripped to /test but was" + stripped)
	}

	stripped = stripEndingSlash("/test/foo")
	if stripped != "/test/foo" {
		t.Errorf("/test/foo should not have been stripped but was" + stripped)
	}

	stripped = stripEndingSlash("/test/foo/")
	if stripped != "/test/foo" {
		t.Errorf("/test/foo/ should have been stripped to /test/foo but was" + stripped)
	}

}

func TestNormalizePath(t *testing.T) {
	var normalized string

	normalized = normalizePath("//")
	if normalized != "/" {
		t.Errorf("The root path has been falsy normalized to " + normalized)
	}

	normalized = normalizePath("/foo//test/")
	if normalized != "/foo/test/" {
		t.Errorf("The /foo//test/ path has been falsy normalized to " + normalized)
	}

	normalized = normalizePath("/foo///test")
	if normalized != "/foo/test" {
		t.Errorf("The /foo///test path has been falsy normalized to " + normalized)
	}	

}

func TestEmbeddingCheck(t *testing.T) {

	type RequiredStruct struct {
		Name string
		Age int
	}

	type EmbeddingStructOk struct {
		RequiredStruct
	}
	
	type EmbeddingStructNotOk1 struct {
		
	}

	type EmbeddingStructNotOk2 struct {
		Name string
		Age int		
	}

	type EmbeddingStructNotOk3 struct {
		Embedded RequiredStruct
	}	

	var requiredStruct RequiredStruct
	var okayStruct EmbeddingStructOk
	var notOkayStruct1 EmbeddingStructNotOk1
	var notOkayStruct2 EmbeddingStructNotOk2
	var notOkayStruct3 EmbeddingStructNotOk3

	var hasEmbedded bool

	hasEmbedded, _ = hasEmbeddedStruct(reflect.TypeOf(requiredStruct),reflect.TypeOf(okayStruct))	
	if !hasEmbedded {
		t.Errorf("The embedded struct was not recognized")
	}

	hasEmbedded, _ = hasEmbeddedStruct(reflect.TypeOf(requiredStruct),reflect.TypeOf(notOkayStruct1))
	if hasEmbedded {
		t.Errorf("The embedded struct was wrongly recognized (1)")
	}	

	hasEmbedded, _ = hasEmbeddedStruct(reflect.TypeOf(requiredStruct),reflect.TypeOf(notOkayStruct2))
	if hasEmbedded {
		t.Errorf("The embedded struct was wrongly recognized (2)")
	}

	hasEmbedded, _ = hasEmbeddedStruct(reflect.TypeOf(requiredStruct),reflect.TypeOf(notOkayStruct3))
	if hasEmbedded {
		t.Errorf("The embedded struct was wrongly recognized (3)")
	}	

}

func TestGetStructPointer(t *testing.T) {
	var ptr interface{}
	var err *SimpleError

	type RequiredStruct struct {
		Name string
		Age int
	}

	type EmbeddingStructOk struct {
		RequiredStruct
		Firstname string
	}
	
	type EmbeddingStructNotOk1 struct {
		
	}

	type EmbeddingStructNotOk2 struct {
		Name string
		Age int		
	}

	type EmbeddingStructNotOk3 struct {
		Embedded RequiredStruct
	}	

	var requiredStruct RequiredStruct
	var okStruct EmbeddingStructOk

	var okayStruct *EmbeddingStructOk

	okayStruct = new(EmbeddingStructOk)
	ptr , err = getStructPointer(reflect.TypeOf(requiredStruct),reflect.ValueOf(okayStruct))
	if err != nil {
		t.Errorf("No error expected")
	}
	ptr.(*RequiredStruct).Name = "foo"
	if (okayStruct.Name != "foo") {
		t.Errorf("No pointer")
	}

	ptr , err = getStructPointer(reflect.TypeOf(okStruct),reflect.ValueOf(okayStruct))
	if err != nil {
		t.Errorf("No error expected")
	}
	ptr.(*EmbeddingStructOk).Firstname = "foo"
	if (okayStruct.Firstname != "foo") {
		t.Errorf("No pointer")
	}

}

func TestExtractParams(t *testing.T) {
	var params []string

	params = extractParams("/test","/foo")
	if params != nil {
		t.Errorf("Params are not NIL")
	}

	params = extractParams("/test","/test2")
	if params != nil {
		t.Errorf("Params are not NIL")
	}	

	params = extractParams("/","//")
	if params == nil || len(params) != 0 {
		t.Errorf("Params are not NIL")
	}

	params = extractParams("/test","/test")
	if params == nil || len(params) != 0 {
		t.Errorf("Params are NIL or empty")
	}

	params = extractParams("/test","/test/")
	if params == nil || len(params) != 0 {
		t.Errorf("Params are NIL or empty")
	}	

	params = extractParams("/test/foo","/test//foo/")
	if params == nil || len(params) != 0 {
		t.Errorf("Params are NIL or empty")
	}	

	params = extractParams("/:test/foo","/value//foo/")
	if params == nil || len(params) != 1 {
		t.Errorf("Params are NIL or not 1")
	}
	if params[0] == "" {
		t.Errorf("Params[0] is empty")	
	} else  if params[0] != "value" {
		t.Errorf("Params[0] test is not value but " + params[0])	
	}

	params = extractParams("/:test/:foo","/testValue//fooVal/")
	if params == nil || len(params) != 2 {
		t.Errorf("Params are NIL or not 2")
	}
	if params[0] == "" {
		t.Errorf("Params[0] test is empty")	
	} else if params[0] != "testValue" {
		t.Errorf("Params[0] test is not testValue but " + params[0])	
	}
	if params[1] == "" {
		t.Errorf("Params[1] foo is empty")	
	} else if params[1] != "fooVal" {
		t.Errorf("Params[1] foo is not fooVal but " + params[1])	
	}

}