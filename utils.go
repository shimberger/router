package router

import (
	"strings"
	"reflect"
	//"fmt"
)

type SimpleError struct {
	message string
}

func (e *SimpleError) Error() string {
	return e.message
}

// Strips the ending slash of a string
func stripEndingSlash(str string) string {
	if len(str) > 1 {
		if str[len(str)-1] == '/' {
			return str[0:len(str)-1]
		}
	}
	return str
}

// Removes all double slashes from a path
func normalizePath(path string) string {
	for strings.Index(path,"//") >= 0 {
		path = strings.Replace(path,"//","/",-1)
	} 
	return path
}

// Checks if the actual type embeds the required type
func hasEmbeddedStruct(required reflect.Type, actual reflect.Type) (bool, int) {
	for i := 0; i < actual.NumField(); i++ {
		t := actual.Field(i)
		if t.Anonymous && t.Type == required {
			return true, i
		}
	}
	return false, 0
}

// Returns a pointer to an embedded struct matching the specified type
func getStructPointer(needle reflect.Type, haystack reflect.Value) (interface{}, *SimpleError) {
	// If the outer struct is already matching, return it
	//fmt.Printf("%v==%v",needle,haystack.Type())
	if (needle == haystack.Elem().Type()) {
		// Return a pointer to the struct
		return haystack.Interface(), nil
	}
	// Now check anonymously embedded structs for the specified type
	for i := 0; i < haystack.Elem().Type().NumField(); i++ {
		field := haystack.Elem().Type().Field(i)
		// If there is an anonymous embedded struct that matches the specified type
		if field.Anonymous && field.Type == needle {
			// Return a pointer to the field
			return haystack.Elem().Field(i).Addr().Interface(), nil
		}
	}
	return reflect.ValueOf(nil), &SimpleError{"Not found"}
}

// Extract's parameters from a path given a spec
func extractParams(pattern string, path string) []string {
	// First explode the normalized path into its elements
	patternParts := strings.Split(stripEndingSlash(normalizePath(pattern)),"/")
	pathParts    := strings.Split(stripEndingSlash(normalizePath(path)),"/")
	// They can only match if the length is the same
	if (len(pathParts) == len(patternParts)) {
		params   := make([]string,0)
		numParts := len(pathParts)
		for i := 1; i < numParts; i++ {
			if (len(patternParts[i]) > 0 && patternParts[i][0] == ':') {
				// If the current pattern part is a variable it is considered a match
				params = append(params,pathParts[i])
			} else if (pathParts[i] != patternParts[i]) {
				// If the current part is not a variable and doess not match we can abort
				return nil
			}
		}
		return params
	}
	return nil
}
