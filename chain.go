package router

type Chain struct {
	counter int // The current chaining index
	errCode int // The error code encountered, if any
	errMsg  string // The error message encountred, if any
	GetFunc func(int,*Chain) func() // A function that returns a function to call for next
}

func NewChain() *Chain {
	return new(Chain)
}

func (c *Chain) Execute() {
	c.counter = 0
	c.errCode = -1
	c.errMsg  = ""
	c.Next()
}

func (c *Chain) Next() {
	if c.errCode != -1 {
		return
	}	
	nextFunc := c.GetFunc(c.counter,c)
	c.counter++
	if nextFunc != nil {
		nextFunc()
	}	
}

func (c *Chain) Abort(errCode int, errMsg string) {
	c.errCode = errCode
	c.errMsg = errMsg
}