package router

import (
	"testing"
)


func TestCallHandler(t *testing.T) {
	var success bool
	var params []string
	ctx := new(HttpContext)
		
	success , _ = callHandler(ctx, make([]string,0), func(c *HttpContext, str string, num int) {})
	if success {
		t.Errorf("Should have returned error")
	}

	params = []string {"name","name"}
	success , _ = callHandler(ctx, params, func(c *HttpContext, str string, num int) {})
	if success {
		t.Errorf("Should have returned error")
	}

	params = []string {"name","2"}
	success , _ = callHandler(ctx, params, func(c *HttpContext, str string, num int) {})
	if !success {
		t.Errorf("Should have returned success")
	}
}

func TestFindHandler(t *testing.T) {
	var params []string
	var foundHandler handler

	handlers := make(map[string] handler)
	handlers["/users"] = func(c *HttpContext, str string, num int) {}
	handlers["/users/:id"] = func(c *HttpContext, str string, num int) {}	
	handlers["/users/:id/profile"] = func(c *HttpContext, str string, num int) {}

	foundHandler, params = findHandler(handlers, "/users/")
	if (foundHandler == nil || len(params) != 0) {
		t.Errorf("Failed (1)")
	}

	foundHandler, params = findHandler(handlers, "/user/")
	if (foundHandler != nil || params != nil) {
		t.Errorf("Failed (2)")
	}

	foundHandler, params = findHandler(handlers, "/users/12")
	if (foundHandler == nil || len(params) != 1 || params[0] != "12") {
		t.Errorf("Failed (3)")
	}	

	foundHandler, params = findHandler(handlers, "/users/12/profile/")
	if (foundHandler == nil || len(params) != 1 || params[0] != "12") {
		t.Errorf("Failed (4)")
	}	

}


