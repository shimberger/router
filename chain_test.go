package router

import (
	"testing"
)

func TestChain(t *testing.T) {

	var called = 0

	var handler = func(c *Chain) {
		called++
		c.Next()
		called++
	}

	var handler2 = func(c *Chain) {
		c.Abort(101,"foo")
	}	

	handlers := make([]interface{},0)
	handlers = append(handlers,handler)
	handlers = append(handlers,handler)
	handlers = append(handlers,handler)
	handlers = append(handlers,handler2)
	handlers = append(handlers,handler)

	chain := NewChain()
	chain.GetFunc = func(i int, c *Chain) func() {
		if i >= len(handlers) {
			return nil
		}
		
		handler := handlers[i].(func(c *Chain))
		return func() {
			handler(c)
		}
	}
	chain.Execute()

	if called != 6 {
		t.Errorf("Called not 6")
	}

	if chain.errCode != 101 {
		t.Errorf("ErrorCode not 101")
	}	

}